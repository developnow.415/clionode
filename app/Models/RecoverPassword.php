<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class RecoverPassword extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'token', 'code', 'is_used'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeIsNotExpired($query)
    {
        $date = new \DateTime;
        $date->modify('-5 minutes');
        $formatted_date = $date->format('Y-m-d H:i:s');

        return $query->where('created_at','>=',$formatted_date);
    }
}
