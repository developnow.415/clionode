<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Node extends Model
{
    protected $fillable = [
        'name',
        'parent_id',
        'owner_id',
        'height',
        'department_name',
        'strongest_language'
    ];


    public function parent()
    {
        return $this->hasOne(Node::class, 'id', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Node::class, 'parent_id', 'id');
    }

    public function owner()
    {
        return $this->hasOne(User::class, 'id', 'owner_id');
    }
}
