<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NodeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'parent' => $this->parent
                ? ['id' => $this->parent->id, 'name' => $this->parent->name]
                : null,
            'height' => $this->height,
            'department_name' => $this->department_name,
            'strongest_language' => $this->strongest_language,
            'owner' => ['id' => $this->owner->id, 'name' => $this->owner->name],
            'children' => ChildNodeResource::collection($this->children)
        ];
    }
}
