<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Models\User;
use App\Models\RecoverPassword;
use App\Mail\SendResetPasswordMail;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Illuminate\Support\Str;


class PasswordResetController extends Controller {
  
    public function forgotPassword(Request $request){
        $email = $request->email;

        // If email does not exist
        if(!$this->validEmail($email)) {
            return response()->json([
                'message' => 'Email does not exist.'
            ], Response::HTTP_NOT_FOUND);
        } else {
            // If email exists
            $token = $this->sendResetPasswordMail($email);
            return response()->json([
                'message' => 'Check your inbox, we have sent the code.',
                'token' => $token
            ], Response::HTTP_OK);            
        }
    }


    public function sendResetPasswordMail($email){
        $data = $this->generateToken($email);
        Mail::to($email)->send(new SendResetPasswordMail($data));
        
        return $data['token'];
    }

    public function validEmail($email) {
       return !!User::where('email', $email)->first();
    }

    public function generateToken($email){
      $token = Str::random(80);;
      $code = random_int(100000, 999999);
      $user = $this->storeToken($token, $email, $code);

      return [
          'user' => $user,
          'code' => $code,
          'token' => $token
      ];
    }

    public function storeToken($token, $email, $code){
        $user = User::where('email', $email)->firstOrFail();
        $user->recoverPassword()->create(compact ('email', 'token', 'code'));

        return $user;
    }

    private function getRecoveryItem(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
            'code' => 'required',
        ]);


        $recovery = RecoverPassword::where('token', $request->token)
            ->where('code', $request->code)
            ->isNotExpired()
            ->first();

        return $recovery;
    }

    public function verifyCode(Request $request)
    {
        $is_not_expired = !!$this->getRecoveryItem($request);

        return response()->json($is_not_expired, $is_not_expired? 200: 404);
    }

    public function resetPassword(Request $request)
    {
        $recovery_item = $this->getRecoveryItem($request);

        if (!!$recovery_item) {


        } else {
            return response()->json([
                'message' => 'Code is expired or invalid'
            ], 422);
        }

        
    }
    
}