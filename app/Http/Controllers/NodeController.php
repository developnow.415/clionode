<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Node;
use App\Models\User;
use App\Http\Resources\NodeResource;
    
class NodeController extends Controller
{
    public function all()
    {
        $nodes = Node::with('children', 'owner', 'parent')->get();
        $fomatted_data = NodeResource::collection($nodes);

        return response()->json($fomatted_data, 200);
    }

    public function getChildNodes($id)
    {
        $nodes = Node::with('children', 'owner', 'parent')
                ->where('parent_id', $id)
                ->get();
                
        $fomatted_data = NodeResource::collection($nodes);

        return response()->json($fomatted_data, 200);
    }

    public function storeNode(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'owner_id' => 'required',
        ]);

        // Check owner's validity
        list ($is_valid, $message) = $this->ownerCheck($request->owner_id);

        if (!$is_valid) {
            return response()->json(compact('message'), 422);
        }


        $data = $request->only(['name', 'parent_id', 'owner_id', 'department_name', 'strongest_language']);
        $data['parent_id'] = !$data['parent_id']? null : $data['parent_id'];

        $parent_id = $request->parent_id;
        $root_node = Node::where('height', 0)->first();
        $parent_node = null;

        // Check if root node exists
        if (!!$root_node) {
            // Check if the parent_id is empty and terminate
            if (!$parent_id) {
                return response()->json(['message' => 'Parent node is required'], 422);
            }

            // Check if the parent exists
            $parent_node = Node::find($parent_id);

            if (!$parent_node) {
                return response()->json(['message' => 'Parent node not found'], 422);
            }

        } else {
            // Check if the parent_id is empty and terminate
            if (!!$parent_id) {
                return response()->json(['message' => 'Root node must be added first with no parent node.'], 422);
            }

        }

        $node_name = $request->name;

        // check if node name matches to the root node
        $node_name_exists = Node::where('name', $node_name)->where('height', 0)->exists();

        if ($node_name_exists) {
            // reject if name is already taken
            return response()->json(['message' => $node_name. ' is already taken.'], 422);
        }

        try {

            // Create and get new node
            $new_node = $this->getCreatedNode($data, $parent_node);

            return response()->json($new_node, 200);

        } catch(\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 422);
        }


    }

    public function changeParentNode($id, Request $request)
    {
        $this->validate($request, [
            'parent_id' => 'required',
        ]);

        $parent_id = $request->parent_id;

        $parent_node = Node::find($parent_id);

        // Check if parent exists
        if (!$parent_node) {
            return response()->json(['message' => 'Parent not found'], 404);
        }

        // Check if the current node is root with height = 0 and reject request
        $node = Node::find($id);

        if (!$node) {
            return response()->json(['message' => 'Node not found'], 404);
        }

        $node->update(['parent_id' => $parent_node->id]);


        return response()->json($node->fresh(), 200);
    }

    private function hasRootNode()
    {
        return !!Node::where('height', 0)->first();
    }

    private function getCreatedNode($data, $parent_node = null)
    {
        // Calculate height of the new node
        $data['height'] = !!$parent_node? $parent_node->height + 1 : 0;

        // retrieve or create new node
        $new_node = Node::create($data);

        return new NodeResource($new_node);
    }

    private function ownerCheck($owner_id)
    {
        $is_valid = true;
        $message = '';

        // Check if node owner exists
        $node_owner_exists = User::find($owner_id);

        if(!$node_owner_exists) {
            // reject request if owner is not found
            $is_valid = false;
            $message = 'Node owner not found';
        }

        // Check if owner has already a node
        $node_owner_exists = Node::where('owner_id', $owner_id)->exists();

        if ($node_owner_exists) {
            // reject request if the owner is already assigned to a node.
            $is_valid = false;
            $message = 'Node owner is already assigned';            
        }


        return [$is_valid, $message];
    }
}
