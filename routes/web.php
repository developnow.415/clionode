<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return 'Clio node api 1.0';
});

$router->get('/api', function () use ($router) {
    return redirect('/');
});

$router->group(['prefix' => 'api'], function () use ($router) {
    
    $router->post('register', 'AuthController@register');
 
    $router->post('login', 'AuthController@login');
    $router->post('logout', 'AuthController@logout');
    $router->post('refresh', 'AuthController@refresh');
    $router->post('me', 'AuthController@me');

    $router->post('forgot-password', 'PasswordResetController@forgotPassword');
    $router->post('reset-password', 'PasswordResetController@resetPassword');
    $router->post('verify-code', 'PasswordResetController@verifyCode');

    // Users
    $router->get('users', 'UserController@all');

    // Clio node endpoints
    $router->get('nodes', 'NodeController@all');
    $router->get('nodes/{id}', 'NodeController@getChildNodes');
    $router->post('nodes', 'NodeController@storeNode');
    $router->put('nodes/{id}', 'NodeController@changeParentNode');
});
