
![Logo](https://www.clio.me/wp-content/uploads/2019/03/clio-logo-black-rgb.png)

    
# Clio Node Exercise

We in Clio, need to model how our company is structured so we can help our new employees have a better overview of our 
company structure.

We have our root node (only one, in our case the CEO) and several child nodes.
Each of these nodes may have its own children. 

It can be structured as something like this: 
```
        root
       /    \
      a      b
      |
      c
    / 	\
   d     e
```

### We need 3 endpoints that will serve basic operations : 
```
0. Add a new node to the tree.
1. Get all child nodes of a given node from the tree. (Just 1 layer of children)
2. Change the parent node of a given node.
```

### Each node should have the following data: 

```
0. Node identifier.
1. node name.
2. who is the parent node.
3. The height of the node. (in the example above `height(root)=0` and `height(a)=1`)
4. Managers should have an extra field specifying the name of the department they are managing.
5. Developers should have an extra field specifying the name of the programming language they are strongest in.
```

## Installation

* Install composer

```bash
  composer install
```

* Copy .env.example as .env and change database credentials and host

* Run database migration

```bash
  php artisan migrate
``` 
## API Reference

#### Get all nodes

```http
  GET /api/nodes
```
200 Response
````
[
    {
        "id": 4,
        "name": "SubCeo",
        "parent": null,
        "height": 0,
        "department_name": null,
        "strongest_language": null,
        "owner": {
            "id": 1,
            "name": "Freva Gatwar"
        },
        "children": [
            {
                "id": 5,
                "name": "Manager",
                "parent": {
                    "id": 4,
                    "name": "Ceo"
                },
                "height": 1,
                "department_name": null,
                "strongest_language": null,
                "owner": {
                    "id": 2,
                    "name": "Jobep Gaerba"
                }
            }
        ]
    },
]
````

#### Get child nodes

```http
  GET /api/nodes/{id}
```
200 Response
````
[
    {
        "id": 4,
        "name": "SubCeo",
        "parent": null,
        "height": 0,
        "department_name": null,
        "strongest_language": null,
        "owner": {
            "id": 1,
            "name": "Freva Gatwar"
        },
        "children": [
            {
                "id": 5,
                "name": "Manager",
                "parent": {
                    "id": 4,
                    "name": "Ceo"
                },
                "height": 1,
                "department_name": null,
                "strongest_language": null,
                "owner": {
                    "id": 2,
                    "name": "Jobep Gaerba"
                }
            }
        ]
    },
]
````
#### Store new node

```http
  POST /api/nodes
```
Body
| Parameter  | Type     | Description                |
| :--------  | :------- | :------------------------- |
| `id`       | `integer`| **Required**. Identifier   |
| `name`     | `string` | **Required**. Node Name    |
| `parent_id`| `integer` | *Nullable*. Parent node id|
| `owner_id` | `integer` | **Requuired**. User id |
| `height`   | `integer` | **Required**. Level of the node|
| `department_name`   | `string` | *Optional*. Department name|
| `strongest_language`   | `string` | *Optional*. Strong language|

200 Response
````
{
    "id": 4,
    "name": "Ceo",
    "parent": null,
    "height": 0,
    "department_name": null,
    "strongest_language": null,
    "owner": {
        "id": 1,
        "name": "Freva Gatwar"
    },
    "children": []
},
````
422 Response
````
{
  "message": "Error message here"
},
````

#### Change parent node

```http
  PUT /api/nodes/{id}
```
Body
| Parameter  | Type     | Description                |
| :--------  | :------- | :------------------------- |
| `parent_id`| `integer` | **Requuired**. Parent node id|

200 Response
````
{
    "id": 4,
    "name": "Ceo",
    "parent": null,
    "height": 0,
    "department_name": null,
    "strongest_language": null,
    "owner": {
        "id": 1,
        "name": "Freva Gatwar"
    },
    "children": []
}
````  
#### Get all users

```http
  GET /api/users
```
200 Response
````
[
    {
        "id": 5,
        "name": "Bravo Van",
        "email": "bvan@gmail.com",
        "mobile": null,
        "email_verified_at": null,
        "active": 0,
        "photo_path": null,
        "created_at": "2021-09-28T22:57:04.000000Z",
        "updated_at": "2021-09-28T22:57:04.000000Z"
    },
    {
        "id": 15,
        "name": "Cosmos Vint",
        "email": "cvint@gmail.com",
        "mobile": null,
        "email_verified_at": null,
        "active": 0,
        "photo_path": null,
        "created_at": "2021-09-28T22:58:53.000000Z",
        "updated_at": "2021-09-28T22:58:53.000000Z"
    }
]
````

## Demo

https://clionode.herokuapp.com